# $Id: makefile 11 2007-04-03 22:25:53Z jpeltier $
# This file seems to originate from something called 'the ultimate
# latex makefile' that different authors worked on. Find original
# versions at:
# http://tadek.pietraszek.org/blog/2006/03/24/the-ultimate-latex-makefile/
# (c) by Tadeusz Pietraszek
# http://www.acoustics.hut.fi/u/mairas/UltimateLatexMakefile/ 
# (c) by Matti Airas
#
# This file has been assembled and adjusted by Steven Bergner, Nov 2005
#  - support for multiple sub-texfiles
#  - fixed '=' '#' problem in mingw/msys environment (see E variable)
#  - PS compilation uses VGTC recommended settings
#  - targets restructured to make PDF by default
#
# This file has been further adjusted by Tom Torsney-Weir, Aug 2012
# - pdflatex is now the only compilation method
#
# Call 'make' for minimal compilation, to check for proper latex
# syntax. This can be much faster than a full PDF generation when big
# figures are involved.
#
#----------------------------------------------------------------------------
#adjust the following for your purpose

#set the a .dvi name that corresponds to your main .tex file
# multiple .dvi files on this line will all be processed
#DVIFILES = template.dvi
MAINTEXFILES = thes-full.tex
SUBTEXFILES = 
#BIBFILES = thes-full.bib

#switch = to # when using ps2pdf in win32 (mingw/msys), i.e. uncomment 2nd line
#E=\=
E=\#

#adjust paper size: letter/a4
PAPERSIZE=letter
PDFLATEXPAPERSIZE="-sPAPERSIZE$E$(PAPERSIZE)"
DVIPSPAPERSIZE=-t $(PAPERSIZE)
#uncomment the following two lines when using option tvcgpapersize
#DVIPSPAPERSIZE=-T 7.875in,10.75in
#PDFLATEXPAPERSIZE=

#----------------------------------------------------------------------------
BIBTEX = bibtex
L2H = latex2html
PDFLATEX = pdflatex

RERUN = "(There were undefined references|Rerun to get (cross-references|the bars) right)"
RERUNBIB = "No file.*\.bbl|Citation.*undefined" 

PDFFILES = $(MAINTEXFILES:.tex=.pdf)

COPY = if test -r $*.toc; then cp $*.toc $*.toc.bak; fi 
#RM = /usr/bin/rm -f 
RM = rm -f 

all:	pdf

pdf: $(PDFFILES)

$(MAINTEXFILES) : $(SUBTEXFILES) $(BIBFILES)

%.pdf: %.tex
	$(COPY);$(PDFLATEX) $<
	egrep -c $(RERUNBIB) $*.log && ($(BIBTEX) $*;$(COPY);$(PDFLATEX) $<) ; true
	egrep $(RERUN) $*.log && ($(COPY);$(PDFLATEX) $<) ; true
	egrep $(RERUN) $*.log && ($(COPY);$(PDFLATEX) $<) ; true
	#if cmp -s $*.toc $*.toc.bak; then . ;else $(PDFLATEX) $< ; fi
	#$(RM) $*.toc.bak
# Display relevant warnings
	egrep -i "(Reference|Citation).*undefined" $*.log ; true

pics:
	cd images; ./convPNGtoPS; cd ..
	cd images; ./convJPGtoPS; cd ..

# cleans anything that can be re-generated automatically, plus emacs backups
clean: 
				rm -f *.aux *.log *.bbl *.blg *.brf *.cb *.ind *.idx *.ilg	\
				*.inx *.toc *.out *.dvi *.lof *.lop *.lot \
				$(PDFFILES) *~

.PHONY : all pdf ps clean $(MAINTEXFILES)
